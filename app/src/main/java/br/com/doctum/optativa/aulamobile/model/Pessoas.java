package br.com.doctum.optativa.aulamobile.model;

import java.io.Serializable;

public class Pessoas implements Serializable {
    private Long id;
    private String nomePessoa;
    private String emailPessoa;
    private String idadePessoa;
    private String telefonePessoa;

    public Long getId() {
        return id;
    }

    @Override
    public String toString(){
       return nomePessoa.toString();
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomePessoa() {
        return nomePessoa;
    }

    public void setNomePessoa(String nomePessoa) {
        this.nomePessoa = nomePessoa;
    }

    public String getEmailPessoa() {
        return emailPessoa;
    }

    public void setEmailPessoa(String emailPessoa) {
        this.emailPessoa = emailPessoa;
    }

    public String getIdadePessoa() {
        return idadePessoa;
    }

    public void setIdadePessoa(String idadePessoa) {
        this.idadePessoa = idadePessoa;
    }

    public String getTelefonePessoa() {
        return telefonePessoa;
    }

    public void setTelefonePessoa(String telefonePessoa) {
        this.telefonePessoa = telefonePessoa;
    }
}

