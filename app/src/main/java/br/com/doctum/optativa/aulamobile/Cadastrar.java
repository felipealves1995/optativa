package br.com.doctum.optativa.aulamobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.doctum.optativa.aulamobile.BancoDados.PessoasBd;
import br.com.doctum.optativa.aulamobile.model.Pessoas;

public class Cadastrar extends LifeCycle {

    EditText campoNome, campoIdade, campoTel, campoEmail;
    Button btSalvar;
    Pessoas pessoa;
    PessoasBd bd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar);

        pessoa = new Pessoas();
        bd = new PessoasBd(Cadastrar.this);

        campoNome = findViewById(R.id.campoNome);
        campoIdade = findViewById(R.id.campoIdade);
        campoEmail = findViewById(R.id.campoEmail);
        campoTel = findViewById(R.id.campoTel);

        btSalvar = findViewById(R.id.btSalvar);
        btSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pessoa.setNomePessoa(campoNome.getText().toString());
                pessoa.setEmailPessoa(campoEmail.getText().toString());
                pessoa.setTelefonePessoa(campoTel.getText().toString());
                pessoa.setIdadePessoa(campoIdade.getText().toString());

                bd.salvarPessoa(pessoa);
                bd.close();
                alert("Pessoa cadastrada com sucesso!");
                campoEmail.setText("");
                campoIdade.setText("");
                campoNome.setText("");
                campoTel.setText("");
            }
        });
    }

    public void alert(String msg) {

        Toast.makeText(Cadastrar.this, msg, Toast.LENGTH_SHORT).show();

    }
}