package br.com.doctum.optativa.aulamobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.doctum.optativa.aulamobile.BancoDados.PessoasBd;
import br.com.doctum.optativa.aulamobile.LifeCycle;
import br.com.doctum.optativa.aulamobile.model.Pessoas;

public class FormularioEditar extends LifeCycle {
    EditText campoNome, campoIdade, campoTel, campoEmail;
    Button btAlterar, btExcluir;
    Pessoas pessoa;
    PessoasBd bd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_editar);
        pessoa = new Pessoas();

        pessoa = (Pessoas) getIntent().getSerializableExtra("pessoa-escolhida");
        pessoa.setNomePessoa("" + getIntent().getSerializableExtra("pessoa-escolhida"));

        bd = new PessoasBd(FormularioEditar.this);

        campoNome = findViewById(R.id.campoNome);
        campoIdade = findViewById(R.id.campoIdade);
        campoEmail = findViewById(R.id.campoEmail);
        campoTel = findViewById(R.id.campoTel);

        campoNome.setText(pessoa.getNomePessoa());
        campoEmail.setText(pessoa.getEmailPessoa());
        campoIdade.setText(pessoa.getIdadePessoa());
        campoTel.setText(pessoa.getTelefonePessoa());
        pessoa.setId(pessoa.getId());

        btAlterar = findViewById(R.id.btAlterar);
        btAlterar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pessoa.setNomePessoa(campoNome.getText().toString());
                pessoa.setEmailPessoa(campoEmail.getText().toString());
                pessoa.setTelefonePessoa(campoTel.getText().toString());
                pessoa.setIdadePessoa(campoIdade.getText().toString());

                bd.alterarPessoas(pessoa);
                bd.close();
                alert("Pessoa editada com sucesso!");
                campoEmail.setText("");
                campoIdade.setText("");
                campoNome.setText("");
                campoTel.setText("");
            }
        });
        btExcluir = findViewById(R.id.btExcluir);
        btExcluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bd.deletarPessoas(pessoa);
                alert("Pessoa excluida com sucesso!");
            }
        });
    }


    public void alert(String msg) {

        Toast.makeText(FormularioEditar.this, msg, Toast.LENGTH_SHORT).show();

    }
}
