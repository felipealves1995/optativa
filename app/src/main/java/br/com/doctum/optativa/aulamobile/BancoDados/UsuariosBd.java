package br.com.doctum.optativa.aulamobile.BancoDados;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import br.com.doctum.optativa.aulamobile.Configuracoes;
import br.com.doctum.optativa.aulamobile.model.Pessoas;
import br.com.doctum.optativa.aulamobile.model.Usuarios;

public class UsuariosBd extends SQLiteOpenHelper {

    private static final String DATABASE = "bd";
    private static final int VERSION = 1;

    public UsuariosBd(Context context) {
        super(context, DATABASE, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String usuarios = "CREATE TABLE usuario(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                "nomeUser TEXT NOT NULL, senhaAtual TEXT NOT NULL, senhaNova TEXT NOT NULL," +
                "senhaConfirm TEXT NOT NULL);";
        db.execSQL(usuarios);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String usuarios = "DROP TABLE IF EXISTS usuarios";
        db.execSQL(usuarios);
    }


    //SALVAR USUARIO

    public void salvarUsuario(Usuarios usuarios) {
        ContentValues values = new ContentValues();

        values.put("nomeUser", usuarios.getNomeUser());
        values.put("senhaAtual",usuarios.getSenhaAtual() );
        values.put("senhaNova",usuarios.getSenhaNova());
        values.put("senhaConfirm",usuarios.getSenhaConfirm() );


        getWritableDatabase().insert("usuarios", null, values);

    }

    //EDITAR USUARIOS

    public void alterarUsuario(Usuarios usuarios) {
        ContentValues values = new ContentValues();

        values.put("nomeUser", usuarios.getNomeUser());
        values.put("senhaAtual",usuarios.getSenhaAtual() );
        values.put("senhaNova",usuarios.getSenhaNova());
        values.put("senhaConfirm",usuarios.getSenhaConfirm() );

        String[] args = {usuarios.getId().toString()};
        getWritableDatabase().update("usuario", values, "id=?", args);
    }

    //DELETAR USUARIOS

    public void deletarUsuario(Usuarios usuarios) {
        String[] args = {usuarios.getId().toString()};
        getWritableDatabase().delete("usuarios", "id=?", args);
    }
    //LISTAR PESSOAS

    public ArrayList<Usuarios> getListar() {
        String[] columns = {"id", "nomeUser", "senhaAtual", "senhaNova", "senhaConfirm"};
        Cursor cursor = getWritableDatabase().query("usuario", columns, null, null, null, null, null, null);
        ArrayList<Usuarios> usuarios = new ArrayList<Usuarios>();

        while (cursor.moveToNext()) {
            Usuarios usuario = new Usuarios();
            usuario.setId(cursor.getLong(0));
            usuario.setNomeUser(cursor.getString(1));
            usuario.setSenhaAtual(cursor.getString(2));
            usuario.setSenhaNova(cursor.getString(3));
            usuario.setSenhaConfirm(cursor.getString(4));

            usuarios.add(usuario);
        }
        return usuarios;
    }
}
