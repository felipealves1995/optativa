package br.com.doctum.optativa.aulamobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import br.com.doctum.optativa.aulamobile.BancoDados.PessoasBd;
import br.com.doctum.optativa.aulamobile.model.Pessoas;

public class Editar extends LifeCycle {


    PessoasBd bd;
    ListView listar;
    ArrayList<Pessoas> listagem;
    Pessoas pessoa;
    ArrayAdapter adapter;
    private Object menuInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);

        listar = (ListView) findViewById(R.id.listagem);
        registerForContextMenu(listar);
        listar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {

                Pessoas pessoaEscolhida = (Pessoas) adapter.getItemAtPosition(position);
                Intent i = new Intent(Editar.this, FormularioEditar.class);
                i.putExtra("pessoa-escolhida", pessoaEscolhida);
                startActivity(i);
            }
        });

        listar.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapter, View view, int position, long id) {
                pessoa = (Pessoas) adapter.getItemAtPosition(position);
                return false;
            }
        });

        carregarLista();

    }

    protected void onResume() {
        super.onResume();
        carregarLista();
    }

    public void carregarLista() {
        bd = new PessoasBd(Editar.this);
        listagem = bd.getListar();
        bd.close();

        if (listagem != null) {
            adapter = new ArrayAdapter<Pessoas>(Editar.this, android.R.layout.simple_list_item_1, listagem);
            listar.setAdapter(adapter);
        }
    }
}
