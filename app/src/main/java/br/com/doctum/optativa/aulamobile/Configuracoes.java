package br.com.doctum.optativa.aulamobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.doctum.optativa.aulamobile.BancoDados.PessoasBd;
import br.com.doctum.optativa.aulamobile.BancoDados.UsuariosBd;
import br.com.doctum.optativa.aulamobile.model.Pessoas;
import br.com.doctum.optativa.aulamobile.model.Usuarios;

public class Configuracoes extends LifeCycle {

    EditText campoUser, senhaAtual, senhaNova, senhaConfirm;
    Button btSalvarUsuario;
    Usuarios usuario;
    UsuariosBd bd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracoes);
        usuario = new Usuarios();
        bd=new UsuariosBd(Configuracoes.this);

        campoUser= findViewById(R.id.campoUser);
        senhaAtual= findViewById(R.id.campoSenhaAtual);
        senhaNova=findViewById(R.id.campoNovaSenha);
        senhaConfirm=findViewById(R.id.campoConfirmaSenha);
        btSalvarUsuario=findViewById(R.id.btSalvarUsuario);
        btSalvarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usuario.setNomeUser(campoUser.getText().toString());
                usuario.setSenhaAtual(senhaAtual.getText().toString());
                usuario.setSenhaNova(senhaNova.getText().toString());
                usuario.setSenhaConfirm(senhaConfirm.getText().toString());

                bd.salvarUsuario(usuario);
                bd.close();
                alert("Usuario cadastrado com sucesso!");
                campoUser.setText("");
                senhaAtual.setText("");
                senhaNova.setText("");
                senhaConfirm.setText("");


            }
        });


    }

    public void alert(String msg) {

        Toast.makeText(Configuracoes.this, msg, Toast.LENGTH_SHORT).show();

    }
}
