package br.com.doctum.optativa.aulamobile.BancoDados;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import br.com.doctum.optativa.aulamobile.model.Pessoas;

public class PessoasBd extends SQLiteOpenHelper {

    private static final String DATABASE = "bd";
    private static final int VERSION = 1;

    public PessoasBd(Context context) {
        super(context, DATABASE, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String pessoas = "CREATE TABLE pessoas(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                "nomePessoa TEXT NOT NULL, emailPessoa TEXT NOT NULL, idadePessoa TEXT NOT NULL," +
                "telefonePessoa TEXT NOT NULL);";
        db.execSQL(pessoas);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String pessoas = "DROP TABLE IF EXISTS pessoas";
        db.execSQL(pessoas);
    }


    //SALVAR PESSOA

    public void salvarPessoa(Pessoas pessoas) {
        ContentValues values = new ContentValues();

        values.put("nomePessoa", pessoas.getNomePessoa());
        values.put("emailPessoa", pessoas.getEmailPessoa());
        values.put("telefonePessoa", pessoas.getTelefonePessoa());
        values.put("idadePessoa", pessoas.getIdadePessoa());


        getWritableDatabase().insert("pessoas", null, values);

    }

    //EDITAR PESSOAS

    public void alterarPessoas(Pessoas pessoas) {
        ContentValues values = new ContentValues();

        values.put("nomePessoa", pessoas.getNomePessoa());
        values.put("emailPessoa", pessoas.getEmailPessoa());
        values.put("telefonePessoa", pessoas.getTelefonePessoa());
        values.put("idadePessoa", pessoas.getIdadePessoa());

        String[] args = {pessoas.getId().toString()};
        getWritableDatabase().update("pessoas", values, "id=?", args);
    }

    //DELETAR PESSOAS

    public void deletarPessoas(Pessoas pessoas) {
        String[] args = {pessoas.getId().toString()};
        getWritableDatabase().delete("pessoas", "id=?", args);
    }
    //LISTAR PESSOAS

    public ArrayList<Pessoas> getListar() {
        String[] columns = {"id", "nomePessoa", "emailPessoa", "telefonePessoa", "idadePessoa"};
        Cursor cursor = getWritableDatabase().query("pessoas", columns, null, null, null, null, null, null);
        ArrayList<Pessoas> pessoas = new ArrayList<Pessoas>();

        while (cursor.moveToNext()) {
            Pessoas pessoa = new Pessoas();
            pessoa.setId(cursor.getLong(0));
            pessoa.setNomePessoa(cursor.getString(1));
            pessoa.setEmailPessoa(cursor.getString(2));
            pessoa.setTelefonePessoa(cursor.getString(3));
            pessoa.setIdadePessoa(cursor.getString(4));


            pessoas.add(pessoa);
        }
        return pessoas;
    }
}
