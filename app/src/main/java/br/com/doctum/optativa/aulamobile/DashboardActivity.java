package br.com.doctum.optativa.aulamobile;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class DashboardActivity extends LifeCycle {

    private ImageView botaoCadastro;
    private ImageView botaoListar;
    private ImageView botaoEditar;
    private ImageView botaoConfig;
    private ImageView botaoWeb;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        botaoWeb = findViewById(R.id.btWebservice);
        botaoWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashboardActivity.this, ListWebActivity.class));
            }
        });


        botaoCadastro = findViewById(R.id.btCad);
        botaoCadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashboardActivity.this, Cadastrar.class));
            }
        });

        botaoListar = findViewById(R.id.btListar);
        botaoListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashboardActivity.this, Listar.class));
            }
        });

        botaoEditar = findViewById(R.id.btEditar);
        botaoEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashboardActivity.this, Editar.class));
            }
        });

        /*botaoConfig = findViewById(R.id.btConfig);
        botaoConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashboardActivity.this, Configuracoes.class));
            }
        });*/

    }
}
