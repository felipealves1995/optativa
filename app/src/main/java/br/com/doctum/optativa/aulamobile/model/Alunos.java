package br.com.doctum.optativa.aulamobile.model;

public class Alunos {
    private String alunos_nome,alunos_email;

    public Alunos() {

    }

    public String getNome_aluno() {
        return alunos_nome;
    }

    public void setAlunos_nome(String alunos_nome) {
        this.alunos_nome = alunos_nome;
    }

    public String getAlunos_email() {
        return alunos_email;
    }

    public void setAlunos_email(String alunos_email) {
        this.alunos_email = alunos_email;
    }
}
