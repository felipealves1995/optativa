package br.com.doctum.optativa.aulamobile;

import android.database.DataSetObserver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import br.com.doctum.optativa.aulamobile.model.Alunos;

public class WebService extends AppCompatActivity {

    Button botaoDados;
    ListView listagemNomeEmail;
    ArrayList<Aluno> listagem;
    ArrayAdapter adapter;

    class Aluno{
        public String alunos_nome;
        public String alunos_email;
    }

    Adapter aa = null;
    static ArrayList<String> resultRow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_service);

        botaoDados=(Button) findViewById(R.id.btDados);
        botaoDados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClassConexao conexao = new ClassConexao();


                try {
                    String resposta = conexao.execute("http://45.55.53.18/aulamobile/webservices/alunos.json").get();
                    JSONArray jsonArray = new JSONArray(resposta);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        Aluno resultRow = new Aluno();

                        resultRow.alunos_nome = jsonObject.getString("alunos_nome");
                        resultRow.alunos_email = jsonObject.getString("alunos_email");
                        adapter.add(resultRow);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (listagem != null) {
                        adapter = new ArrayAdapter<Aluno>(WebService.this, android.R.layout.simple_list_item_2, listagem);
                        listagemNomeEmail.setAdapter(adapter);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

            }
        });


    }






}
