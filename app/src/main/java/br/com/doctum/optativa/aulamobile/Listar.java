package br.com.doctum.optativa.aulamobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import br.com.doctum.optativa.aulamobile.BancoDados.PessoasBd;
import br.com.doctum.optativa.aulamobile.model.Pessoas;

public class Listar extends LifeCycle {

    PessoasBd bd;
    ListView listar;
    ArrayList<Pessoas> listagem;
    Pessoas pessoa;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);

        listar = (ListView) findViewById(R.id.listagem);
        carregarLista();

    }

    public void carregarLista() {
        bd = new PessoasBd(Listar.this);
        listagem = bd.getListar();
        bd.close();

        if (listagem != null) {
            adapter = new ArrayAdapter<Pessoas>(Listar.this, android.R.layout.simple_list_item_1, listagem);
            listar.setAdapter(adapter);
        }
    }
}
